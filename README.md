# CDW Explanation



## Getting started

The two projects you should look at are [rsync approach](https://gitlab.com/gitlab-learn-labs/instruqt-dev-space/rsync-pipelines/rysnc-approach) and [rsync test app](https://gitlab.com/gitlab-learn-labs/instruqt-dev-space/rysnc-test-app). The other project in this repo (dependent pipelines) is an alternative approach if you dont want to have you dev branch just be a copy of the prod branch

## Rsync Approach Project

This is a basic set up that corresponds to a prod + dev Instruqt course. Both prod and dev projects are just a pull from the Instruqt side and run a test job that check the ***rsync test app*** project (which we will explain more in the next section). The only variable you should need here is the instruqt token, and besides that the main purpose of this project is version control. We also run the pipeline nightly so ensure that any temp changes made in the Instruqt ui are overwritten. The last thing to note in the project is in the ***solve-workstation*** file you will have to change the project id value to be the id of your testing project.

## Rsync Test App Project

This project is ideally the end result of whatever you want to test. Most of our courses revolve around pipelines and deployments, so we will make different branches to match state and have them run every day. That way we have a record of if the pipelines successfully ran which alerts us when something breaks and the pipelines fail. The ***solve-workstation*** file in the other project runs a script that checks the last two days of pipeline runs in this project and fails the deployment if something has broken. That way you are always alerted if you are pushing out a change while your application is broken, and alerted daily if something is broken because of your scheduled pipelines.

## Known Limitations

The flaw with this system is when you are trying to push out a fix when you know something is wrong the fix will always be blocked. What we do is just temporarily remove the checking script to push it out, or you could delete the failed pipelines as well.
